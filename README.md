# CREATON Visualisierungsplattform API v1

### [https://www.dach-checker.de/api/v1/](https://www.dach-checker.de/api/v1/)

# Table of Contents

1. [Request](#markdown-header-request)
1. [Authentication](#markdown-header-authentication)
1. [Response](#markdown-header-response)
1. [Fetch](#markdown-header-fetch)
    - [request](#markdown-header-request)
    - [request:filter](#markdown-header-requestfilter)
    - [request:response](#markdown-header-requestresponse)
1. [Update](#markdown-header-update)
1. [Reject](#markdown-header-reject)
1. [Status-Ids](#markdown-header-status-ids)

# Request

The requests are designed as simple `x-www-form-urlencoded` requests. For better readability, the required key-value pairs are shown as JSON below.

An full example request message could look like this:
```text
POST /api/v1/request HTTP/1.1
Content-Type: application/x-www-form-urlencoded; charset=utf-8
Host: www.dach-checker.de
Connection: close
User-Agent: Paw/3.2 (Macintosh; OS X/10.16.0) GCDHTTPRequest
Content-Length: 79

_token=randomToken&_user=user%40mail
```

# Authentication

For a successfull request to this API you have to add the keys `_token` and `_user` as POST parameters.

```json
{
    "_token": "randomToken",
    "_user": "user@mail"
}
```

The required values can be found within the Visualisierungsplatform under [Settings > Schnittstelle](https://www.dach-checker.de/settings?tab=api). *(Only available for logged in users)*

# Response

All requests will return a JSON. Within each response you will find a key called status containing a 200 when succesfull.

If the execution failed you will get a corresponding status code equivalent to HTML status codes. Additional informations can be found within `message` like here:

```json
{
    "status": 401,
    "message": "Request could not be authenticated."
}
```


# Fetch

Returns all request contained within the Visualisierungsplattform.

```
[POST] https://www.dach-checker.de/api/v1/request
```

## request

Aside form the validation keys no additional parameters are neccesary. All available requests will be returned like shown in [request:response](#request:response).

```json
{
    "_token": "randomToken",
    "_user": "user@mail"
}
```

## request:filter

You can also filter the returned requests via the `filter` attribute. You can filter for **id** and / or **status**. Here are all filters listed:

> #### Note:
> Accepted status values for `filter[status]` are `1`, `2`, `3` and `4`.
> 
> _Not all status filter values may be accessable for your user._

```json
{
    "_token": "randomToken",
    "_user": "user@mail",
    "filter": {
        "status": 1,
        "id": 1,
    }
}
```

## request:response

The resposne will contain all requests within the data-array:

> #### Note:
> - The key-value pairs for `product_code`, `product_model` and `product_color` are deprecated.
> - For `pv_system.orientation` the possible values are `""`, `"horizontal"` and `"vertical"`.
> - For `pv_system.type` the possible values are `""`, `"applied"` and `"integrated"`.

```json
{
    "status": 200,
    "count": 1,
    "data": [
        {
            "id": 84,
            "status_id": "4",
            "user_group_id": "3",
            "created_at": "2019-04-08 07:43:04",
            "updated_at": "2019-04-15 06:30:53",
            "images": {
                "original": [
                    "https:\/\/www.dach-checker.de\/storage\/84\/original\/0.jpeg"
                ],
                "edited": [
                    "https:\/\/www.dach-checker.de\/storage\/84\/edited\/0.png"
                ]
            },
            "status": {
                "id": 4,
                "title": "denied",
                "position": "400",
                "created_at": "2019-02-04 09:07:23",
                "updated_at": "2019-02-04 09:07:23",
                "name": "Abgelehnt"
            },
            "history": {
                "id": 32,
                "request_id": "84",
                "status_id": "1",
                "new_status_id": "4",
                "user_id": "1",
                "user_group_id": "1",
                "message": "Dieses Bild wurde aus diesem Grund abgelehnt.",
                "created_at": "2019-04-15 06:30:53",
                "updated_at": "2019-04-15 06:30:53",
                "user_name": "Kommdirekt"
            },
            "pv_system": {
                "height": "2",
                "orientation": "horizontal",
                "type": "integrated",
                "width": "3"
            },
            "products": [
                {
                    "code": "610-501-1000",
                    "color": "rot glasiert",
                    "model": "FUTURA",
                    "name": "FUTURA Flächenziegel FINESSE rot glasiert"
                },
                {
                    "code": "610-507-1000",
                    "color": "weinrot glasiert",
                    "model": "FUTURA",
                    "name": "FUTURA Flächenziegel FINESSE weinrot glasiert"
                }
            ]
        },
        // ...
    ]
}
```

# Update

Allows you to upload an image via the API:

```
[POST] https://www.dach-checker.de/api/v1/update/{ID}
```

## Anfrage

```json
{
    "_token": "randomToken",
    "_user": "user@mail",
    "images": [
        "data:image/jpg;base64,..."
    ]
}
```

## Response

```json
{
    "status": "200",
    "message": "success"
}
```

# Reject

Allows you to reject a response via the API:

```
[POST] https://www.dach-checker.de/api/v1/reject/{ID}
```

## Anfrage

```json
{
    "_token": "randomToken",
    "_user": "user@mail",
    "message": "Ablehnungsgrund als String."
}
```

## Response

```json
{
    "status": "200",
    "message": "success"
}
```

# Status-IDs

|ID|Meaning|
|--|---------|
|1|New Request|
|2|For Release|
|3|Done|
|4|Rejected|